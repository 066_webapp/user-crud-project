import { useMessageStore } from "./message";
import { useUserStore } from "./user";
import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useLoginStore = defineStore("login", () => {
  const loginName = ref("");
  const isLogin = computed(() => {
    return loginName.value !== "";
  });
  const userStore = useUserStore();
  const messageStore = useMessageStore();
  const login = (userName: string, password: string): void => {
    if (userStore.login(userName, password)) {
      loginName.value = userName;
      localStorage.setItem("loginName", userName);
    } else {
      messageStore.showMessage("Login หรือ Password ไม่ถูกต้อง");
    }
  };
  const logout = (): void => {
    loginName.value = "";
    localStorage.removeItem("loginName");
  };

  const loadData = () => {
    loginName.value = localStorage.getItem("loginName") || "";
  };

  return { loginName, isLogin, login, logout, loadData };
});
